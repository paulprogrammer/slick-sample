
name         := "slick"

organization := "com.example"

version      := "1.0"

scalaVersion := "2.11.7"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += "spray repo" at "http://repo.spray.io"

libraryDependencies ++= {
  val scalaTestV  = "2.2.5"
  val scalazV     = "7.1.4"
  val slickV      = "3.0.3"
  val scalaLogV   = "3.1.0"
  val logbackV    = "1.1.3"
  val h2V         = "1.4.189"
  Seq(
    "org.scalaz"                 %% "scalaz-core"           % scalazV,
    "org.scalatest"              %% "scalatest"             % scalaTestV % "test",
    "com.typesafe.slick"         %% "slick" 		            % slickV,
    "com.zaxxer"                 %  "HikariCP"              % "2.4.1",
    "com.typesafe.scala-logging" %% "scala-logging"         % scalaLogV,
    "io.spray"                   %% "spray-can"             % "1.3.3",
    "io.spray"                   %% "spray-routing"         % "1.3.3",
    "com.typesafe.akka"          %% "akka-actor"            % "2.4.0",
    "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.6.1",
    "ch.qos.logback"             %  "logback-classic"       % logbackV,
    "com.h2database"             %  "h2"                    % h2V,
    "org.mariadb.jdbc"           %  "mariadb-java-client"   % "1.3.0-beta-2"
  )
} 
