
Once you've cloned the project:

```
$ sbt

> compile

> test
```

In src/test/com/samples/DatabaseSpec

The test creates a coderz table in memory and adds two records, the test checks that the Future eventually returns two records.

This code uses:   scala 2.11.7


