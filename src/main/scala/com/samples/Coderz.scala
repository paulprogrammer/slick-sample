package com.samples

import slick.backend.DatabaseConfig
import slick.dbio.SuccessAction
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._
import slick.ast._
import slick.jdbc.meta.MTable

import scala.concurrent.Await
import scala.concurrent.duration.FiniteDuration

class Coderz(tag: slick.driver.MySQLDriver.api.Tag) extends Table[(Int, String)](tag, "CODERZ") {
  def id = column[Int]("CODER_ID", ColumnOption.PrimaryKey, ColumnOption.AutoInc ) // This is the primary key column
  def name = column[String]("CODER_NAME")
  // Every table needs a * projection with the same type as the table's type parameter
  def * = (id, name)
}


/**
 * Created by paulw on 10/9/15.
 */
object Coderz {

  import scala.concurrent.ExecutionContext.Implicits.global

  var db:Database = DatabaseConfig.forConfig[JdbcProfile]("mysql").db

  def bootstrap = {
    db.createSession().withTransaction(
      Await.ready(db.run(createIfNotExist), FiniteDuration(1,"hour"))
    )
  }

  bootstrap

  val coderz = TableQuery[Coderz]

  val tablesExist: DBIO[Boolean] = MTable.getTables.map { tables =>
    val names = Vector(coderz.baseTableRow.tableName)
    names.intersect(tables.map(_.name.name)) == names
  }

  val createIfNotExist: DBIO[Unit] =
    tablesExist.flatMap(exist => if (exist) SuccessAction{} else coderz.schema.create)

  def add(who:String):Unit = {
    Await.result(db.run(coderz.map(coder => (coder.id, coder.name)) ++= Seq((0,who))),
          FiniteDuration(1,"hour"))
  }

  def del(id:Int):Unit = {
    db.run(coderz.filter(_.id === id).delete)
  }

  def get(id:Int):Unit = {
    coderz.filter(_.id === id)
  }

  def list() = {
    val q = for (c <- coderz) yield (c.id, c.name)
    val a = q.result
    Await.result(db.run(a), FiniteDuration(1,"hour"))
  }
}
