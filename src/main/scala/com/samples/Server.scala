package com.samples

import akka.actor.ActorSystem
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import spray.routing.SimpleRoutingApp

/**
 * Created by paulw on 10/9/15.
 */
object Server extends App with SimpleRoutingApp {
  implicit val system = ActorSystem("my-system")

  object JacksonWrapper {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)

    def serialize(value: Any): String = {
      import java.io.StringWriter
      val writer = new StringWriter()
      mapper.writeValue(writer, value)
      writer.toString
    }
  }

  Coderz.bootstrap

  startServer(interface = "localhost", port = 8080) {
    path("hello") {
      get {
        complete {
          <h1>Say hello to spray</h1>
        }
      }
    }

   path("dev") {
      get {
        complete {
          JacksonWrapper serialize Coderz.list()
        }
      }
    } ~
    pathPrefix(RestPath) { who =>
      put {
        complete {
          Coderz.add(who.toString())
          "\"Success\""
        }
      }
    }
  }
}
