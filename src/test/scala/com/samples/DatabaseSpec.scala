package com.samples

import org.scalatest.Matchers
import com.typesafe.scalalogging.LazyLogging
import org.scalatest._
import slick.driver.H2Driver.api._
import scala.util.Try
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile
import slick.jdbc.meta.MTable
import slick.dbio.SuccessAction
import org.scalatest.concurrent.ScalaFutures
import scala.concurrent.Future
import org.scalatest.time.Span
import org.scalatest.time.Seconds
import org.scalatest.time.Millis

class Coderz(tag: slick.driver.H2Driver.api.Tag) extends Table[(Int, String)](tag, "CODERZ") {
  def id = column[Int]("CODER_ID", O.PrimaryKey) // This is the primary key column
  def name = column[String]("CODER_NAME")
  // Every table needs a * projection with the same type as the table's type parameter
  def * = (id, name)
}


class SampleDatabaseSpec extends FlatSpec with Matchers with LazyLogging with ScalaFutures {
 
  import scala.concurrent.ExecutionContext.Implicits.global
  
  val coderz = TableQuery[Coderz]

  def connect(f: Database => Unit ) = {
    val dbConfig = DatabaseConfig.forConfig[JdbcProfile]("h2")
    f(dbConfig.db)
  }

  val tablesExist: DBIO[Boolean] = MTable.getTables.map { tables =>
    val names = Vector(coderz.baseTableRow.tableName)
    names.intersect(tables.map(_.name.name)) == names
  }
  
  val create: DBIO[Unit] = (coderz.schema).create
  
  val createIfNotExist: DBIO[Unit] = 
    tablesExist.flatMap(exist => if (exist) SuccessAction{} else create)

  val insertCoderz: DBIO[Option[Int]] = 
    coderz.map(coder => (coder.id, coder.name)) ++= Seq(
        (1, "Paul The Mad Programmer"), 
        (2, "Andy Madder For Listening Slack")
      )

  val reloadData: DBIO[Option[Int]]  = 
    coderz.delete >> insertCoderz

  val listCoderz: DBIO[Seq[String]] = 
    coderz.map(_.name).result

    
  def bootstrap(db:Database) = db.run(createIfNotExist >> reloadData >> listCoderz)
  
  implicit override val patienceConfig =
    PatienceConfig(timeout = scaled(Span(2, Seconds)), interval = scaled(Span(5, Millis)))

  "db" should "have two loonies" in {
    connect { db =>
      whenReady(bootstrap(db)) { results =>
        results should be (Seq("Paul The Mad Programmer","Andy Madder For Listening Slack"))
      }
    }
  }
  
}